;;; kitchingroup-44.el --- Identifying Potential \ce{BO2} Oxide Polymorphs for Epitaxial Growth Candidates

;; Copyright (C) 2016 John Kitchin

;; Version: 0.0.1
;; Author: John Kitchin <jkitchin@andrew.cmu.edu>
;; Keywords:
;; DOI: 10.1021/am4059149
;; Journal: ACS Appl. Mater. Interfaces
;; Bibtex: @article{mehta-2015-ident-poten,
;;   author =	 {Prateek Mehta and Paul A. Salvador and John R. Kitchin},
;;   title =	 {Identifying Potential \ce{BO2} Oxide Polymorphs for Epitaxial
;;                   Growth Candidates},
;;   journal =	 {ACS Appl. Mater. Interfaces},
;;   volume =	 6,
;;   number =	 5,
;;   pages =	 {3630-3639},
;;   year =	 2015,
;;   doi =		 {10.1021/am4059149},
;;   url =		 {http://dx.doi.org/10.1021/am4059149},
;;   eprint =	 {http://pubs.acs.org/doi/pdf/10.1021/am4059149},
;;   keywords =	 {orgmode},
;;  }


;;; Commentary:



;;; Code:
(require 'cappa)

(cappa-register 'kitchingroup-44)


(provide 'kitchingroup-44)
;;; kitchingroup-44.el ends here
